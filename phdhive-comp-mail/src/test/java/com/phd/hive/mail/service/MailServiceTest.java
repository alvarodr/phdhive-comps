package com.phd.hive.mail.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phd.hive.mail.IMailService;
import com.phd.hive.mail.base.BaseTestSpring;
import com.phd.hive.mail.model.EmailModel;
import com.phd.hive.mail.model.EmailRecipients;


public class MailServiceTest extends BaseTestSpring {
	
	@Autowired
	IMailService mailService;
	
	@Test
	public void test() {
		EmailModel model = new EmailModel("newUser");
		EmailRecipients rc = new EmailRecipients();
		rc.addTo("alvarodr1983@gmail.com");
		model.setRecipients(rc);
		mailService.sendMailOnTransaction(model, true);
	}

}
