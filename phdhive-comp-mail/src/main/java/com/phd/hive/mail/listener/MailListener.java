package com.phd.hive.mail.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.phd.hive.mail.exception.MailException;
import com.phd.hive.mail.impl.IMailSender;
import com.phd.hive.mail.model.EmailData;

/**
 * 
 * @author ADORU3N
 *
 */
@Component("mailListener")
public class MailListener implements MessageListener {

	private Logger logger = LoggerFactory.getLogger(MailListener.class);
	
	@Autowired
	private IMailSender mailSenderService;
	
	@Override
	public void onMessage(Message msg) {
		EmailData mailData = null;
		try {
			mailData = (EmailData) ((ObjectMessage) msg).getObject();
			logger.info("Enviando mail a los destinatarios {}", mailData.getRecipients().getTo());
		
			mailSenderService.send(mailData);
		} catch (JMSException e) {
			logger.error("[ERROR] en la cola JMS", e);
		} catch (MailException e) {
			logger.error("[ERROR] enviando mail a {}", mailData.getRecipients().getTo());
			logger.error("[ERROR] al enviar el mail", e);
		}
	}

}
