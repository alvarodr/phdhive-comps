package com.phd.hive.mail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class EmailRecipients implements Serializable {

	private static final long serialVersionUID = 8729661012920534471L;
	
	private static final String MAIL_SEPARATOR_STR = ";";

	private static final char MAIL_SEPARATOR_CHAR = ';';

	private List<String> to;

	private String from = "";

	private List<String> bcc;

	private List<String> cc;

	/**
	 * Agrega una direccion de copia oculta
	 * 
	 * @param bcc
	 *            direccion
	 */
	public void addBcc(String bcc) {
		if (StringUtils.isBlank(bcc)) {
			return;
		}
		if (this.bcc == null) {
			this.bcc = new ArrayList<String>();
		}
		fillList(this.bcc, bcc);
	}

	/**
	 * Agrega varias direcciones de copia oculta
	 * 
	 * @param bcc
	 *            direcciones
	 */
	public void addBcc(String... bccs) {
		if (bccs == null) {
			return;
		}
		for (String bcc : bccs) {
			addBcc(bcc);
		}
	}

	/**
	 * Agrega una direccion de copia
	 * 
	 * @param cc
	 *            direccion
	 */
	public void addCc(String cc) {
		if (StringUtils.isBlank(cc)) {
			return;
		}
		if (this.cc == null) {
			this.cc = new ArrayList<String>();
		}
		fillList(this.cc, cc);
	}

	/**
	 * Agrega una direccion de copia
	 * 
	 * @param cc
	 *            direcciones
	 */
	public void addCc(String... ccs) {
		if (ccs == null) {
			return;
		}
		for (String cc : ccs) {
			addCc(cc);
		}

	}

	/**
	 * Agrega destinatarios del correo
	 * 
	 * @param tos
	 *            destinatarios
	 */
	public void addTo(String... tos) {
		if (tos == null) {
			return;
		}
		for (String to : tos) {
			addTo(to);
		}

	}

	/**
	 * Agrega un destinatario del correo
	 * 
	 * @param tos
	 *            destinatario
	 */
	public void addTo(String to) {
		if (StringUtils.isBlank(to)) {
			return;
		}
		if (this.to == null) {
			this.to = new ArrayList<String>();
		}
		fillList(this.to, to);
	}

	/**
	 * Obtiene las direcciones de copia oculta
	 * 
	 * @return
	 */
	public String getBcc() {
		if (bcc == null) {
			return "";
		}
		return StringUtils.join(bcc, MAIL_SEPARATOR_CHAR);
	}

	/**
	 * Obtiene las direcciones de copia
	 * 
	 * @return
	 */
	public String getCc() {
		if (cc == null) {
			return "";
		}
		return StringUtils.join(cc, MAIL_SEPARATOR_CHAR);
	}

	/**
	 * Obtiene la direccion de origen del correo
	 * 
	 * @return
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Obtiene las direcciones de destino
	 * 
	 * @return
	 */
	public String getTo() {
		if (to == null) {
			return "";
		}
		return StringUtils.join(to, MAIL_SEPARATOR_CHAR);
	}

	/**
	 * Asigna las direcciones de copia oculta
	 * 
	 * @param cc
	 *            copias de correo ocultas
	 */
	public void setBcc(String bcc) {
		if (StringUtils.isNotBlank(bcc)) {
			this.bcc = new ArrayList<String>();
			fillList(this.bcc, bcc);
		} else {
			this.bcc = null;
		}
	}

	/**
	 * Asigna las direcciones de copia
	 * 
	 * @param cc
	 *            copias de correo
	 */
	public void setCc(String cc) {
		if (StringUtils.isNotBlank(cc)) {
			this.cc = new ArrayList<String>();
			fillList(this.cc, cc);
		} else {
			this.cc = null;
		}
	}

	/**
	 * Asigna el origen
	 * 
	 * @param from
	 *            origen del correo
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Asigna el destinatario
	 * 
	 * @param to
	 *            destinatario del correo
	 */
	public void setTo(String to) {
		if (StringUtils.isNotBlank(to)) {
			this.to = new ArrayList<String>();
			fillList(this.to, to);
		} else {
			this.to = null;
		}
	}

	private void fillList(List<String> list, String mails) {
		if (mails.contains(MAIL_SEPARATOR_STR)) {
			String[] m = mails.split(MAIL_SEPARATOR_STR);
			for (String mail : m) {
				if (StringUtils.isNotBlank(mail)) {
					list.add(mail);
				}
			}
		} else {
			list.add(mails);
		}
	}

}
