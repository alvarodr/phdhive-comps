package com.phd.hive.mail.creator.resources;

import java.net.URL;

import javax.activation.URLDataSource;

import com.phd.hive.mail.util.mime.IContentTypeResolver;

/**
 * 
 * @author ADORU3N
 *
 */
public class URLWithCTDataSource extends URLDataSource {

	private IContentTypeResolver contentTypeResolver;

	public URLWithCTDataSource(URL url, IContentTypeResolver resolver) {
		super(url);
		this.contentTypeResolver = resolver;
	}

	@Override
	public String getContentType() {
		if (contentTypeResolver != null) {
			String ct = contentTypeResolver.getContentTypeByName(getURL().getFile());
			if (ct != null) {
				return ct;
			}
		}
		return super.getContentType();
	}
	
}
