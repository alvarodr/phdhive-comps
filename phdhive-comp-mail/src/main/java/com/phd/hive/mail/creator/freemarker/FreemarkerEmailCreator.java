package com.phd.hive.mail.creator.freemarker;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.phd.hive.mail.creator.resources.IResourceResolver;
import com.phd.hive.mail.model.EmailModel;

import freemarker.cache.TemplateLoader;
import freemarker.core.Environment;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * 
 * @author ADORU3N
 *
 */
@Component("freemarkerEmailCreator")
public class FreemarkerEmailCreator extends BaseEmailCreator<EmailModel> implements InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(FreemarkerEmailCreator.class);
	
	/** Fichero autoimportado */
	public static final String MAGIC_AUTOIMPORT = "templates/_templates_import_.ftl";

	@Autowired
	@Qualifier("applicationConfig")
	private Properties configuration;

	@Value("#{applicationConfig['mail.templates.defaultEncoding']?:'ISO-8859-15'}")
	private String defaultEncoding = "ISO-8859-15";

	@Autowired
	@Qualifier("mainMailFreemarkerConfiguration")
	private Configuration freemarkerConfiguration;

	@Autowired
	private IResourceResolver resourceResolver;

	public FreemarkerEmailCreator() {
		super();
	}

	public void afterPropertiesSet() throws Exception {

		if (freemarkerConfiguration == null) {
			freemarkerConfiguration = new Configuration();
		}

		if (configuration == null) {
			configuration = new Properties();
		}

		freemarkerConfiguration.setLocalizedLookup(false);
		freemarkerConfiguration.setTagSyntax(Configuration.SQUARE_BRACKET_TAG_SYNTAX);
		freemarkerConfiguration.setDefaultEncoding(defaultEncoding);

		freemarkerConfiguration.setAutoFlush(true);
		freemarkerConfiguration.setURLEscapingCharset("UTF-8");

		// Shared variables
		freemarkerConfiguration.setSharedVariable("subject", new SubjectTemplateDirectiveModel());
		freemarkerConfiguration.setSharedVariable("staticAttachment", new AttachmentTemplateDirectiveModel());
		freemarkerConfiguration.setSharedVariable("cidFor", new CidForTemplateDirectiveModel());
		freemarkerConfiguration.setSharedVariable("configuration", configuration);

		freemarkerConfiguration.addAutoImport("templates", MAGIC_AUTOIMPORT);

		freemarkerConfiguration.setDateFormat("dd/MM/yyyy");
		freemarkerConfiguration.setTimeFormat("hh:mm:ss");
		freemarkerConfiguration.setDateTimeFormat("dd/MM/yyyy hh:mm:ss");

	}

	public boolean createMail(MimeMessage message, EmailModel data) throws MessagingException {

		if (data == null || StringUtils.isBlank(data.getTemplateName())) {
			return false;
		}
		MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
		try {
			fillRecipients(helper, data);

			fillBodyAndSubject(helper, data);

			fillAttachments(helper, data);

		} catch (Exception ex) {
			throw new MessagingException("No se pudo crear el contenido del mensaje", ex);
		}
		return true;
	}

	private void fillBodyAndSubject(MimeMessageHelper helper, EmailModel data) throws IOException, TemplateException,
			MessagingException {

		Template t = freemarkerConfiguration.getTemplate(data.getTemplateName() + ".ftl");

		Map<String, Object> freemarkerModel = new HashMap<String, Object>();
		// Fill model
		{
			freemarkerModel.putAll(data.getModel());
			freemarkerModel.put("recipients", data.getRecipients());

			Map<String, Object> ctx = new TreeMap<String, Object>();
			freemarkerModel.put("ctx", ctx);
			// Fill ctx
			{
				ctx.put("fechaGeneracion", new Date());

				ctx.put("template_name", t.getName());
				ctx.put("template_date", null);

				TemplateLoader loader = freemarkerConfiguration.getTemplateLoader();
				Object source = loader.findTemplateSource(t.getName());
				if (source != null) {
					long lastModified = loader.getLastModified(source);
					loader.closeTemplateSource(source);
					ctx.put("template_date", new Date(lastModified));
				}
			}
		}

		StringWriter writer = new StringWriter();
		try {
			Environment env = t.createProcessingEnvironment(freemarkerModel, writer);

			env.process();

			// Set body
			helper.setText(writer.getBuffer().toString(), true);

			// Set Subject
			String subject = (String) env.getCustomAttribute(SubjectTemplateDirectiveModel.CUSTOM_ATTRIBUTE_SUBJECT);
			if (StringUtils.isNotBlank(subject)) {
				helper.setSubject(subject);
			}
			// Inline resources
			CidMappings mappings = (CidMappings) env
					.getCustomAttribute(CidForTemplateDirectiveModel.CUSTOM_ATTRIBUTE_MAPPINGS);
			if (mappings != null) {
				for (CidMapping mapping : mappings.getCids()) {
					DataSource dataSource = resourceResolver.resolveResource(mapping.getResource());
					if (dataSource != null) {
						helper.addInline(mapping.getCid(), dataSource);
					} else {
						LOGGER.warn("No se puede agregar recurso inline a correo {} : no se encuentra",
								mapping.getResource());
					}
				}
			}
			// static Attachments
			Attachments attachments = (Attachments) env
					.getCustomAttribute(AttachmentTemplateDirectiveModel.CUSTOM_ATTRIBUTE_ATTACHMENTS);
			if (attachments != null) {
				for (Attachment attachment : attachments.getAttachments()) {
					DataSource dataSource = resourceResolver.resolveResource(attachment.getResource());
					if (dataSource != null) {
						String name = attachment.getName();
						if (StringUtils.isBlank(name)) {
							name = "";
						}
						helper.addAttachment(name, dataSource);
					} else {
						LOGGER.warn("No se puede agregar adjunto estatico a correo {} : no se encuentra",
								attachment.getResource());
					}
				}
			}

		} finally {
			try {
				writer.close();
			} catch (Exception e) {

			}
		}

	}

	public Properties getConfiguration() {
		return configuration;
	}

	public Configuration getFreemarkerConfiguration() {
		return freemarkerConfiguration;
	}

	public IResourceResolver getResourceResolver() {
		return resourceResolver;
	}

	public void setConfiguration(Properties configuration) {
		this.configuration = configuration;
	}

	public void setDefaultEncoding(String defaultEncoding) {
		this.defaultEncoding = defaultEncoding;
	}

	public void setFreemarkerConfiguration(Configuration configuration) {
		this.freemarkerConfiguration = configuration;
	}

	public void setResourceResolver(IResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

}
