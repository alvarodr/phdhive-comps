package com.phd.hive.mail.model;

import java.io.Serializable;

import javax.activation.DataSource;

/**
 * Adjunto de un correo
 * 
 * @author ADORU3N
 * 
 */
public class EmailAttachment implements Serializable {

	private static final long serialVersionUID = 5830349874255260842L;

	private String name;

	/**
	 * Crea un adjuto
	 * 
	 * @param name
	 *            nombre del fichero en el correo
	 * @param source
	 *            fuente del fichero
	 */
	public EmailAttachment(String name, DataSource source) {
		super();
		this.name = name;
		this.source = source;
	}

	private DataSource source;

	/**
	 * Obtiene el nombre que tendra el adjunto en el correo (Ej:
	 * MiDocumento.pdf)
	 * 
	 * @return nombre
	 */
	public String getName() {
		return name;
	}

	/**
	 * Obtiene la fuente del correo
	 * 
	 * @return fuente
	 */
	public DataSource getSource() {
		return source;
	}
}
