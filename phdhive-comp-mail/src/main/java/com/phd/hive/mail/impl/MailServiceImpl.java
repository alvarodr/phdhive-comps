package com.phd.hive.mail.impl;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.transaction.Status;
import javax.transaction.Synchronization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.phd.hive.arch.transaction.sync.TxSynchronizationException;
import com.phd.hive.arch.transaction.sync.TxSynchronizationManager;
import com.phd.hive.mail.IMailService;
import com.phd.hive.mail.exception.MailException;
import com.phd.hive.mail.model.EmailData;

/**
 * 
 * @author ADORU3N
 *
 */
@Service("mailServiceProduce")
public class MailServiceImpl implements IMailService {
	
	private class MailTxSynchronization implements Synchronization {

		private EmailData mail;

		private boolean onlyOnCommit = false;

		public MailTxSynchronization(EmailData mail, boolean onlyOnCommit) {
			super();
			this.mail = mail;
			this.onlyOnCommit = onlyOnCommit;
		}

		public void afterCompletion(int status) {
			if (onlyOnCommit && status != Status.STATUS_COMMITTED) {
				return;
			}

			enqueueMail(this.mail);
		}

		public void beforeCompletion() {
		}
	}
		
	private static final Logger LOGGER = LoggerFactory.getLogger(MailServiceImpl.class);
	private static final String QUEUE_INCOMING = "incoming.queue";
	
	@Autowired(required = false)
	private TxSynchronizationManager txSynchronizationManager;
	
	@Autowired
	private IMailSender mailSenderService;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	private void enqueueMail(EmailData mail) {
		LOGGER.debug("Correo enviado a la cola de envio : {}", mail);
		try {
			ConnectionFactory cf = jmsTemplate.getConnectionFactory();
			Connection connection = cf.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
			jmsTemplate.convertAndSend(QUEUE_INCOMING, mail);
			// close everything
			session.close();
			connection.close();
		} catch (JMSException e) {
			LOGGER.error("Se ha producido un error JMS en el envio del mail : {}", mail);
		}
	}
	
	public void sendMail(EmailData correo) {
		sendMailOnTransaction(correo, true);
	}

	public <T extends EmailData> void sendMailAsync(T correo) {
		if (correo == null) {
			return;
		}
		enqueueMail(correo);
	}

	public <T extends EmailData> void sendMailInThread(T correo) throws MailException {
		if (correo == null) {
			return;
		}
		mailSenderService.send(correo);
	}

	public void sendMailOnTransaction(EmailData correo, boolean onlyOnCommit) {
		if (correo == null) {
			return;
		}

		try {
			if (txSynchronizationManager != null && txSynchronizationManager.isActive()) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Correo PLANIFICADO DESPUES DE COMMIT {}: {} ",
						(onlyOnCommit ? "(solo en comit exitoso)" : ""), correo);
				}
				txSynchronizationManager.registerSynchronization(new MailTxSynchronization(correo, onlyOnCommit));
				return;
			}

		} catch (TxSynchronizationException ex) {
			// Ignore and continue
		}
		enqueueMail(correo);
	}

	public void setMailSenderService(IMailSender mailSenderService) {
		this.mailSenderService = mailSenderService;
	}

	public void setTxSynchronizationManager(TxSynchronizationManager txSynchronizationManager) {
		this.txSynchronizationManager = txSynchronizationManager;
	};

}
