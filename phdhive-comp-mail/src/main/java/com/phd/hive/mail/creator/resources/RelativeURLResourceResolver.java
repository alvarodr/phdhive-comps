package com.phd.hive.mail.creator.resources;

import java.net.URL;

import javax.activation.DataSource;

import com.phd.hive.mail.util.mime.IContentTypeResolver;
import com.phd.hive.mail.util.mime.NullContentTypeResolver;

/**
 * 
 * @author ADORU3N
 *
 */
public class RelativeURLResourceResolver implements IResourceResolver {

	private IContentTypeResolver contentTypeResolver = NullContentTypeResolver.INSTANCE;

	private URL baseUrl;

	public boolean exists(String name) {
		return true;
	}

	public URL getBaseUrl() {
		return baseUrl;
	}

	public IContentTypeResolver getContentTypeResolver() {
		return contentTypeResolver;
	}

	public DataSource resolveResource(String name) {

		try {
			URL url = new URL(baseUrl, name);
			URLWithCTDataSource urlDataSource = new URLWithCTDataSource(url, contentTypeResolver);
			return urlDataSource;
		} catch (Exception ex) {
			return null;
		}
	}

	public void setBaseUrl(URL base) {
		this.baseUrl = base;
	}

	public void setContentTypeResolver(IContentTypeResolver contentTypeResolver) {
		if (contentTypeResolver == null) {
			this.contentTypeResolver = NullContentTypeResolver.INSTANCE;
		} else {
			this.contentTypeResolver = contentTypeResolver;
		}
	}

}
