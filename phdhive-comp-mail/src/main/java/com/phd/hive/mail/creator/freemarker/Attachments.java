package com.phd.hive.mail.creator.freemarker;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 
 * @author ADORU3N
 *
 */
public class Attachments {

	private Set<Attachment> attachments = new LinkedHashSet<Attachment>();

	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}

	public void addAttachment(Attachment attachment) {
		if (attachment != null) {
			attachments.add(attachment);
		}
	}
	
}
