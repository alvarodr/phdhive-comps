package com.phd.hive.mail.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Clase contenedora de atributos de un contenido.
 * 
 * @author ADORU3N
 * 
 */
public class ContentMetaAttributes implements Serializable {

	private static final long serialVersionUID = 451692732088847433L;

	private Map<String, Object> attributes;
	/**
	 * Categoria por defecto si no se indica<BR>
	 */
	public static final String CATEGORY_DEFAULT = "default";

	/**
	 * Nombre de atributo para categoria del contenido.<BR>
	 * Util para clasificar los contenidos a la hora de almacenar/extraer los
	 * datos de un proveedor de contenido. (String)
	 */
	public static final String ATT_CATEGORY = "category";

	/**
	 * Nombre de atributo para fecha de ultima modificacion de un contenido.<BR>
	 * Util para clasificar los contenidos a la hora de almacenar/extraer los
	 * datos de un proveedor de contenido. (java.util.Date)
	 */
	public static final String ATT_LAST_MODIFIED = "lastModified";

	/**
	 * Nombre de atributo para el nombre original del fichero. (String)<BR>
	 */
	public static final String ATT_ORIGINAL_FILENAME = "originalFilename";

	/**
	 * Nombre de atributo para el tipo MIME del contenido. (String)<BR>
	 */
	public static final String ATT_MIME_TYPE = "mimeType";

	/**
	 * Nombre de atributo para la longitud del contenido. (Long)<BR>
	 */
	public static final String ATT_LENGTH = "length";

	/**
	 * Nombre de atributo para el id del contenido. (String)<BR>
	 */
	public static final String ATT_ID = "id";

	public Object getAttribute(String name) {
		if (attributes == null) {
			return null;
		}
		return attributes.get(name);
	}

	@SuppressWarnings("unchecked")
	public <T extends Object> T getAttribute(String name, Class<T> returnedClass) {
		if (attributes == null) {
			return null;
		}

		Object o = attributes.get(name);
		if (o == null) {
			return null;
		}
		if (!returnedClass.isAssignableFrom(o.getClass())) {
			return null;
		}
		return (T) o;
	}

	public void setAttribute(String name, Object value) {
		if (attributes == null) {
			attributes = new TreeMap<String, Object>();
		}
		if (value != null) {
			attributes.put(name, value);
		} else {
			attributes.remove(name);
		}
	}

	public Map<String, Object> getAttributesMap() {
		return attributes;
	}

	public Set<String> attributeNames() {
		if (attributes == null) {
			return Collections.emptySet();
		}
		return attributes.keySet();
	}

	@Override
	public String toString() {
		return attributes.toString();
	}
}
