package com.phd.hive.mail.creator;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.phd.hive.mail.model.EmailData;

/**
 * 
 * @author ADORU3N
 *
 * @param <T>
 */
public interface IEmailCreator<T extends EmailData> {

	/**
	 * Crea el correo
	 * 
	 * @param message
	 *            mensaje de correo vacio
	 * @param data
	 *            datos para rellenar el correo
	 * @return true si hay que enviar el mensaje, false si no
	 * @throws MessagingException
	 */
	boolean createMail(MimeMessage message, T data) throws MessagingException;
	
}
