package com.phd.hive.mail.dto;

import java.io.IOException;
import java.io.InputStream;

/**
 * 
 * Datos de un contenido.<BR>
 * metadatos + contenido
 * 
 */
@SuppressWarnings("serial")
public abstract class Content extends ContentMeta {
	
	public Content() {
		super();
	}

	public Content(ContentMetaAttributes atts) {
		super(atts);
		setId(atts.getAttribute(ContentMetaAttributes.ATT_ID, String.class));
	}

	public Content(ContentMeta meta) {
		super(meta.getAttributes());
		setId(meta.getId());
	}

	public abstract InputStream getInputStream() throws IOException;
	
}
