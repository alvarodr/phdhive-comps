package com.phd.hive.mail.exception;

/**
 * Exception mail manage.
 * 
 * @author ADORU3N
 *
 */
public class MailException extends Exception {

	private static final long serialVersionUID = -3205722412604406961L;

	public MailException() {
		super();
	}

	public MailException(String message, Throwable cause) {
		super(message, cause);
	}

	public MailException(String message) {
		super(message);
	}

	public MailException(Throwable cause) {
		super(cause);
	}
}
