package com.phd.hive.mail.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.activation.DataSource;

import org.apache.commons.lang.StringUtils;

import com.phd.hive.mail.dto.Content;
import com.phd.hive.mail.support.ContentDataSource;

/**
 * Modelo que representa un correo.
 * 
 * @author ADORU3N
 *
 */
public class EmailModel implements EmailData {

	private static final long serialVersionUID = 3974491574505537493L;

	private List<String> debugInfo;

	private Map<String, Object> model = new TreeMap<String, Object>();

	private EmailRecipients recipients = new EmailRecipients();

	private String templateName;

	private Map<String, Object> unmodifiableModel = Collections.unmodifiableMap(model);

	List<EmailAttachment> attachments = new ArrayList<EmailAttachment>(0);

	/**
	 * Crea un correo sin especificar nombre de plantilla de correo.<BR>
	 * Posteriormente habra que asignar la plantilla de correo mediante el
	 * metodo {@link EmailModel#setTemplateName(String)}
	 */
	public EmailModel() {
		super();
	}

	/**
	 * Crea un correo especificando el nombre de la plantilla que se usara para
	 * crearlo
	 * 
	 * @param templateName
	 *            nombre de la plantilla
	 */
	public EmailModel(String templateName) {
		super();
		this.templateName = templateName;
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Boolean value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Byte value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Character value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Double value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Float value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Integer value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Long value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Object value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, Short value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public void add(String name, String value) {
		model.put(name, value);
	}

	/**
	 * Agrega un valor con nombre al modelo
	 * 
	 * @param name
	 *            nombre
	 * @param value
	 *            valor
	 */
	public <T extends Enum<?>> void add(String name, T value) {
		model.put(name, value);
	}

	/**
	 * Adjunta un documento
	 * 
	 * @param nombre
	 *            nombre que tendra el documento adjunto
	 * @param datasource
	 *            fuente del contenido del documento
	 */
	public void addAttachment(String nombre, DataSource datasource) {
		attachments.add(new EmailAttachment(nombre, datasource));
	}

	/**
	 * Adjunta un documento
	 * 
	 * @param nombre
	 *            nombre que tendra el documento adjunto
	 * @param content
	 *            el contenido del documento
	 */
	public void addAttachment(String nombre, Content content) {
		attachments.add(new EmailAttachment(nombre, new ContentDataSource(content)));
	}

	/**
	 * Agrega un texto que se utilizara en las trazas de depuracion para poder
	 * trazar mas facilmente el correo
	 * 
	 * @param info
	 *            texto de depurecion
	 */
	public void addDebugInfo(String info) {
		if (debugInfo == null) {
			debugInfo = new ArrayList<String>();
		}
		debugInfo.add(info);
	}

	/**
	 * Obtiene un valor del modelo dado un nombre
	 * 
	 * @param name
	 *            nombre
	 * @return el valor en el modelo
	 */
	public Object get(String name) {
		return model.get(name);
	}

	/**
	 * Obtiene un valor del modelo dado un nombre
	 * 
	 * @param name
	 *            nombre
	 * @return el valor en el modelo
	 */
	@SuppressWarnings("unchecked")
	public <T> T get(String name, Class<? extends T> clazz) {
		return (T) model.get(name);
	}

	/**
	 * Obtiene la lista de adjuntos
	 * 
	 * @return lista de adjuntos
	 */
	public List<EmailAttachment> getAttachments() {
		return attachments;
	}

	/**
	 * Obtiene una representacion del modelo como un {@link Map}
	 * 
	 * @return {@link Map} con los objetos del modelo
	 */
	public Map<String, Object> getModel() {
		return unmodifiableModel;
	}

	/**
	 * Obtiene los recipientes del correo
	 * 
	 * @return los recipientes de correo
	 */
	public EmailRecipients getRecipients() {
		return recipients;
	}

	/**
	 * Obtiene el nombre de la plantilla que genera el correo
	 * 
	 * @return
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * Elimina un dato del modelo dado un nombre
	 * 
	 * @param name
	 *            nombre
	 */
	public void remove(String name) {
		model.remove(name);
	}

	/**
	 * Asigna los recipientes del correo
	 * 
	 * @param recipients
	 *            recipientes
	 * */
	public void setRecipients(EmailRecipients recipients) {
		this.recipients = recipients;
	}

	/**
	 * 
	 * Asigna el nombre de la plantilla generadora del correo
	 * 
	 * @param templateName
	 *            nombre de la plantilla
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("EmailModel: ");

		sb.append("template: ");
		sb.append(templateName);

		if (debugInfo != null && !debugInfo.isEmpty()) {
			sb.append(", INFO: ");
			boolean first = true;
			for (String debug : debugInfo) {
				if (first) {
					first = false;
				} else {
					sb.append(", ");
				}
				sb.append("[");
				sb.append(debug);
				sb.append("]");
			}
		}

		if (StringUtils.isNotBlank(this.recipients.getFrom())) {
			sb.append(", DE: [");
			sb.append(this.recipients.getFrom());
			sb.append("], ");
		}

		sb.append(", PARA: [");
		sb.append(this.recipients.getTo());
		sb.append("]");

		sb.append(", HASH: ");
		sb.append(Integer.toHexString(hashCode()));

		return sb.toString();
	}

}
