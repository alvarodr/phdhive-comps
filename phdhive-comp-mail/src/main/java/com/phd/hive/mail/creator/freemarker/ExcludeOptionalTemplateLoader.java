package com.phd.hive.mail.creator.freemarker;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Set;
import java.util.TreeSet;

import freemarker.cache.TemplateLoader;

/**
 * 
 * @author ADORU3N
 *
 */
public class ExcludeOptionalTemplateLoader implements TemplateLoader{

	private Set<String> optional = new TreeSet<String>();
	
	public Set<String> getOptional() {
		return optional;
	}

	public void setOptional(Set<String> optional) {
		this.optional = optional;
	}
	
	public ExcludeOptionalTemplateLoader() {
		super();
		optional.add(FreemarkerEmailCreator.MAGIC_AUTOIMPORT) ;
	}
	
	@Override
	public Object findTemplateSource(String name) throws IOException {
		if(optional.contains(name)) {
			return name ;
		}
		return null;
	}

	@Override
	public long getLastModified(Object templateSource) {
		return 0;
	}

	@Override
	public Reader getReader(Object templateSource, String encoding)	throws IOException {
		return new StringReader("");
	}

	@Override
	public void closeTemplateSource(Object templateSource) throws IOException {
		
	}

}
