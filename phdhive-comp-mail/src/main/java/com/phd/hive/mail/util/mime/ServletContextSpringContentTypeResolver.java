package com.phd.hive.mail.util.mime;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.ServletContextAware;

/**
 * 
 * @author ADORU3N
 *
 */
public class ServletContextSpringContentTypeResolver implements IContentTypeResolver, ServletContextAware {

	private ServletContext context;

	public void setServletContext(ServletContext ctx) {
		this.context = ctx;

	}

	public String getContentTypeByName(String resourceName) {

		String contentType = context.getMimeType(resourceName);
		
		if (contentType == null) {
			contentType = context.getMimeType(StringUtils.lowerCase(resourceName));
		}
		
		if (contentType == null) {
			contentType = context.getMimeType(StringUtils.upperCase(resourceName));
		}
		
		return contentType;
	}

}
