package com.phd.hive.mail.impl;

import org.apache.activeio.xnet.ServiceException;

import com.phd.hive.mail.exception.MailException;
import com.phd.hive.mail.model.EmailData;

public interface IMailSender {
	/**
	 * Crea y envia un correo. Si no puede lanza una excepcion
	 * 
	 * @param correo
	 * @throws ServiceException
	 *             Si no puede enviar el correo
	 */
	<T extends EmailData> void send(T correo) throws MailException;
}
