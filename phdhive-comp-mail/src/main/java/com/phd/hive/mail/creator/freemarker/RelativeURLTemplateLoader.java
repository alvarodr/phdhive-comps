package com.phd.hive.mail.creator.freemarker;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import freemarker.cache.URLTemplateLoader;

public class RelativeURLTemplateLoader extends URLTemplateLoader {

	private URL baseUrl;

	@Override
	public Object findTemplateSource(String name) throws IOException {
		Object o;
		if (name.endsWith(FreemarkerEmailCreator.MAGIC_AUTOIMPORT)) {
			try {

				o = super.findTemplateSource(name);
			} catch (IOException ex) {
				// Ignoramos fallos en el autoimport
				o = null;
			}
		} else {
			o = super.findTemplateSource(name);
		}
		return o;
	}

	public URL getBaseUrl() {
		return baseUrl;
	}

	@Override
	protected URL getURL(String part) {
		try {
			URL otherUrl = new URL(baseUrl, part);

			return otherUrl;
		} catch (MalformedURLException ex) {

		}
		return null;
	}

	public void setBaseUrl(URL base) {
		this.baseUrl = base;
		if (!base.getPath().endsWith("/")) {
			try {
				this.baseUrl = new URL(base.toExternalForm() + "/");
			} catch (MalformedURLException e) {
				// do nothing: handle exception
			}
		}

	}
}
