package com.phd.hive.mail.util.mime;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class MapContentTypeResolver implements IContentTypeResolver {

	@Autowired(required = false)
	@Qualifier("mimeMap")
	private Map<String, String> mimeByExtensionMap;

	public Map<String, String> getMimeByExtensionMap() {
		return mimeByExtensionMap;
	}

	public void setMimeByExtensionMap(Map<String, String> mimeByExtensionMap) {
		this.mimeByExtensionMap = mimeByExtensionMap;
	}

	public String getContentTypeByName(String resourceName) {
		if (mimeByExtensionMap == null || StringUtils.isBlank(resourceName)) {
			return null;
		}
		int pos = resourceName.lastIndexOf('.');
		if (pos == -1) {
			// No ext
			return null;
		}
		String ext = resourceName.substring(pos + 1);
		if (StringUtils.isBlank(ext)) {
			// empty ext
			return null;
		}
		String type = mimeByExtensionMap.get(ext);

		if (StringUtils.isBlank(type)) {
			type = mimeByExtensionMap.get(ext.toUpperCase());
			if (StringUtils.isBlank(type)) {
				type = mimeByExtensionMap.get(ext.toLowerCase());
				if (StringUtils.isBlank(type)) {
					return null;
				}
			}
		}

		return type;
	}
		
}
