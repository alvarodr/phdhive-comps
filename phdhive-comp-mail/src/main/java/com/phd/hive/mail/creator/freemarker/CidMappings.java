package com.phd.hive.mail.creator.freemarker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

/**
 * 
 * @author ADORU3N
 *
 */
public class CidMappings {

	private Map<String, CidMapping> mappingsByResource = new TreeMap<String, CidMapping>();

	private Set<String> cids = new TreeSet<String>();

	private String generateCidForResource(String resource) {
		String cid;
		do {
			cid = UUID.randomUUID() + "." + resource;
		} while (cids.contains(cid));
		cids.add(cid);
		return cid;
	}

	public CidMapping addResource(String resource) {
		CidMapping mapping = mappingsByResource.get(resource);
		if (mapping == null) {

			String cid = generateCidForResource(resource);
			mapping = new CidMapping(cid, resource);
			mappingsByResource.put(resource, mapping);
		}
		return mapping;

	}

	public List<CidMapping> getCids() {
		List<CidMapping> cids = new ArrayList<CidMapping>(mappingsByResource.values());
		return cids;

	}
	
}
