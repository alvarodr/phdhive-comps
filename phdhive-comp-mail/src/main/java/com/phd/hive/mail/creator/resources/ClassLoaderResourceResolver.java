package com.phd.hive.mail.creator.resources;

import java.net.URL;

import javax.activation.DataSource;

import com.phd.hive.mail.creator.freemarker.ClassLoaderTemplateLoader;
import com.phd.hive.mail.util.mime.IContentTypeResolver;
import com.phd.hive.mail.util.mime.NullContentTypeResolver;

/**
 * 
 * @author ADORU3N
 * 
 */
public class ClassLoaderResourceResolver implements IResourceResolver {

	private IContentTypeResolver contentTypeResolver = NullContentTypeResolver.INSTANCE;

	private ClassLoader classLoader = ClassLoaderTemplateLoader.class.getClassLoader();

	private String basePath = "/";

	public boolean exists(String name) {
		String complete = buildUrl(name);
		URL url = classLoader.getResource(complete);
		if (url == null) {
			return false;
		}
		return true;
	}

	public IContentTypeResolver getContentTypeResolver() {
		return contentTypeResolver;
	}

	private String buildUrl(String name) {
		String complete;
		if (name.startsWith("/")) {
			complete = basePath + name.substring(1);
		} else {
			complete = basePath + name;
		}
		return complete;
	}

	public DataSource resolveResource(String part) {

		try {
			String complete = buildUrl(part);
			URL url = classLoader.getResource(complete);
			if (url == null) {
				return null;
			}
			URLWithCTDataSource urlDataSource = new URLWithCTDataSource(url, contentTypeResolver);
			return urlDataSource;
		} catch (Exception ex) {
			return null;
		}
	}

	public void setContentTypeResolver(IContentTypeResolver contentTypeResolver) {
		if (contentTypeResolver == null) {
			this.contentTypeResolver = NullContentTypeResolver.INSTANCE;
		} else {
			this.contentTypeResolver = contentTypeResolver;
		}
	}

	public void setBasePath(String basePath) {
		if (basePath.endsWith("/")) {
			this.basePath = basePath;
		} else {
			this.basePath = basePath + "/";
		}
	}

	public void setClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

}
