package com.phd.hive.mail.creator.freemarker;

import java.net.URL;

import freemarker.cache.URLTemplateLoader;

public class ClassLoaderTemplateLoader extends URLTemplateLoader {

	private ClassLoader classLoader = ClassLoaderTemplateLoader.class.getClassLoader();

	private String basePath = "/";

	private String buildUrl(String name) {
		String complete;
		if (name.startsWith("/")) {
			complete = basePath + name.substring(1);
		} else {
			complete = basePath + name;
		}
		return complete;
	}

	@Override
	protected URL getURL(String name) {

		String complete = buildUrl(name);
		return classLoader.getResource(complete);
	}

	public void setBasePath(String basePath) {
		if (basePath.endsWith("/")) {
			this.basePath = basePath;
		} else {
			this.basePath = basePath + "/";
		}

	}

	public void setClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

}
