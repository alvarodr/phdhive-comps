package com.phd.hive.mail.creator.freemarker;

public class CidMapping {

	private String cid;

	private String resource;
	
	public CidMapping(String cid, String resource) {
		super();
		this.cid = cid;
		this.resource = resource;
	}

	public String getCid() {
		return cid;
	}

	public String getResource() {
		return resource;
	}
	
}
