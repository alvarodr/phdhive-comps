package com.phd.hive.mail.creator.freemarker;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateScalarModel;

/**
 * 
 * @author ADORU3N
 *
 */
public class AttachmentTemplateDirectiveModel implements TemplateDirectiveModel {

	public static final String CUSTOM_ATTRIBUTE_ATTACHMENTS = "__ATTACHMENTS__";

	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopvars, TemplateDirectiveBody body) throws TemplateException, IOException {
		Object resourceParam = params.get("resource");
		String resource = null;
		if (resourceParam != null) {
			if (resourceParam instanceof TemplateScalarModel) {
				TemplateScalarModel model = (TemplateScalarModel) resourceParam;
				String val = model.getAsString();
				if (StringUtils.isNotBlank(val)) {
					resource = val.trim();
				}
			}
		}
		Object nameParam = params.get("name");
		String name = null;
		if (nameParam != null) {
			if (nameParam instanceof TemplateScalarModel) {
				TemplateScalarModel model = (TemplateScalarModel) nameParam;
				String val = model.getAsString();
				if (StringUtils.isNotBlank(val)) {
					name = val.trim();
				}
			}
		}

		if (StringUtils.isBlank(resource)) {
			return;
		}

		Attachments attachments = (Attachments) env.getCustomAttribute(CUSTOM_ATTRIBUTE_ATTACHMENTS);
		if (attachments == null) {
			attachments = new Attachments();
			env.setCustomAttribute(CUSTOM_ATTRIBUTE_ATTACHMENTS, attachments);
		}

		attachments.addAttachment(new Attachment(resource, name));

	}

}
