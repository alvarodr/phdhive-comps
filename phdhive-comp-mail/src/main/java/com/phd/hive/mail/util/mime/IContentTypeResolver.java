package com.phd.hive.mail.util.mime;

/**
 * 
 * @author ADORU3N
 *
 */
public interface IContentTypeResolver {

	/**
	 * 
	 * @param resourceName
	 * @return
	 */
	String getContentTypeByName(String resourceName);
	
}
