package com.phd.hive.mail.creator.freemarker;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateScalarModel;

/**
 * 
 * @author ADORU3N
 *
 */
public class CidForTemplateDirectiveModel implements TemplateDirectiveModel {

	public static final String CUSTOM_ATTRIBUTE_MAPPINGS = "__CID_mappings__";

	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopvars, TemplateDirectiveBody body)
			throws TemplateException, IOException {

		Object resourceParam = params.get("resource");
		String resource = null;
		if (resourceParam != null) {
			if (resourceParam instanceof TemplateScalarModel) {
				TemplateScalarModel model = (TemplateScalarModel) resourceParam;
				String val = model.getAsString();
				if (StringUtils.isNotBlank(val)) {
					resource = val.trim();
				}
			}
		}
		if (resource == null) {
			throw new TemplateModelException("Falta el parametro 'resource' para la macro 'cidFor'");
		}

		CidMappings mappings = (CidMappings) env.getCustomAttribute(CUSTOM_ATTRIBUTE_MAPPINGS);
		if (mappings == null) {
			mappings = new CidMappings();
			env.setCustomAttribute(CUSTOM_ATTRIBUTE_MAPPINGS, mappings);
		}

		CidMapping mapping = mappings.addResource(resource);
		env.getOut().write("cid:" + mapping.getCid());

	}

}
