package com.phd.hive.mail.creator.freemarker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.phd.hive.mail.creator.IEmailCreator;
import com.phd.hive.mail.model.EmailAttachment;
import com.phd.hive.mail.model.EmailData;
import com.phd.hive.mail.model.EmailRecipients;
import com.phd.hive.mail.util.MailUtils;

/**
 * 
 * @author ADORU3N
 *
 * @param <MailModel>
 */
public abstract class BaseEmailCreator<MailModel extends EmailData> implements IEmailCreator<MailModel> {
	protected void fillAttachments(MimeMessageHelper helper, EmailData data) throws MessagingException {

		List<EmailAttachment> attachments = data.getAttachments();
		if (attachments != null) {
			for (EmailAttachment att : attachments) {
				helper.addAttachment(att.getName(), att.getSource()) ;
			}
		}
	}

	protected void fillRecipients(MimeMessageHelper helper, EmailData data) throws MessagingException {

		EmailRecipients correo = data.getRecipients();

		// FROM
		{
			String from = correo.getFrom();
			if (StringUtils.isNotBlank(from)) {
				helper.setFrom(from);
			}
		}

		// TO
		{
			List<String> toList = MailUtils.mailStringToList(correo.getTo());
			String[] p = new String[toList.size()];
			p = toList.toArray(p);

			helper.setTo(p);
		}

		// COPIA OCULTA
		{
			List<String> cc = new ArrayList<String>();
			if (correo.getCc() != null) {
				cc.addAll(MailUtils.mailStringToList(correo.getCc()));
			}

			if (!cc.isEmpty()) {
				// Control de duplicados
				Set<String> ccSet = new HashSet<String>(cc);
				String[] s = ccSet.toArray(new String[ccSet.size()]);
				Arrays.sort(s);

				// Asignacion
				helper.setCc(s);
			}
		}

		// COPIA OCULTA
		{
			List<String> bcc = new ArrayList<String>();
			if (correo.getBcc() != null) {
				bcc.addAll(MailUtils.mailStringToList(correo.getBcc()));
			}

			if (!bcc.isEmpty()) {
				// Control de duplicados
				Set<String> bccSet = new HashSet<String>(bcc);
				String[] s = bccSet.toArray(new String[bccSet.size()]);
				Arrays.sort(s);

				// Asignacion
				helper.setBcc(s);
			}
		}

	}
}
