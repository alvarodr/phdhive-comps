package com.phd.hive.mail.util.mime;

public class NullContentTypeResolver implements IContentTypeResolver {

	private NullContentTypeResolver() {}

	public static final NullContentTypeResolver INSTANCE = new NullContentTypeResolver();

	public String getContentTypeByName(String resourceName) {
		return null;
	}

}
