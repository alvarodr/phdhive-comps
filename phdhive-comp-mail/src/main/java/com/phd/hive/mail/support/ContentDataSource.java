package com.phd.hive.mail.support;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.DataSource;

import com.phd.hive.mail.dto.Content;

public class ContentDataSource implements DataSource {

	private Content content;

	public InputStream getInputStream() throws IOException {
		return content.getInputStream();
	}

	public OutputStream getOutputStream() throws IOException {

		throw new IOException("Not writable");
	}

	public ContentDataSource(Content content) {
		super();
		this.content = content;
	}

	public String getContentType() {
		return content.getMimeType();
	}

	public String getName() {
		return content.getOriginalName();
	}

}

