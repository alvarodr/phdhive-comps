package com.phd.hive.mail.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author ADORU3N
 *
 */
public final class MailUtils {

	private MailUtils() {

	}

	public static List<String> mailStringToList(String string) {
		List<String> list = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(string, ";");
		while (st.hasMoreTokens()) {
			String str = st.nextToken();
			if (StringUtils.isNotBlank(str)) {
				list.add(str.trim());
			}
		}
		return list;
	}

	public static String getPartAsText(MimeMultipart mm,
			String contentTypeFilter) throws MessagingException, IOException {
		BodyPart bp = MailUtils.getPart(mm, contentTypeFilter);
		if (bp != null) {

			Object o = bp.getContent();

			return o.toString();
		}
		return "";
	}

	public static BodyPart getPart(MimeMultipart mm, String contentTypeFilter)
			throws MessagingException, IOException {
		int parts = mm.getCount();
		for (int i = 0; i < parts; i++) {
			BodyPart p = mm.getBodyPart(i);

			String ct;
			Object content;
			DataHandler dh = p.getDataHandler();
			if (dh != null) {
				ct = dh.getContentType();
				content = dh.getContent();
			} else {
				ct = p.getContentType();
				content = p.getContent();
			}

			if (ct.contains("multipart")) {
				MimeMultipart mp = (MimeMultipart) content;
				return getPart(mp, contentTypeFilter);
			} else if (ct.contains(contentTypeFilter)) {
				// Content
				return p;
			}
		}
		return null;
	}
}
