package com.phd.hive.mail.creator.resources;

import javax.activation.DataSource;

/**
 * 
 * @author ADORU3N
 *
 */
public interface IResourceResolver {
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	boolean exists(String name);

	/**
	 * 
	 * @param name
	 * @return
	 */
	DataSource resolveResource(String name);
	
}
