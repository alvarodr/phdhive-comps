package com.phd.hive.mail.model;

import java.io.Serializable;
import java.util.List;
/**
 * Datos para el envio de un correo
 * 
 * @author ADORU3N
 */
public interface EmailData extends Serializable {
	/**
	 * Obtiene los recipientes del correo
	 * 
	 * @return reicipentes
	 */
	EmailRecipients getRecipients();

	/**
	 * Obtiene los adjuntos del correo
	 * 
	 * @return adjuntos
	 */
	List<EmailAttachment> getAttachments();
}
