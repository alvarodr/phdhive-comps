package com.phd.hive.mail.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.activeio.xnet.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.phd.hive.mail.creator.IEmailCreator;
import com.phd.hive.mail.exception.MailException;
import com.phd.hive.mail.model.EmailData;

/**
 * 
 * @author ADORU3N
 *
 */
@Component
public class MailSenderImpl implements IMailSender, ApplicationContextAware {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailSenderImpl.class);

	private Map<Class<EmailData>, IEmailCreator<EmailData>> creators = new HashMap<Class<EmailData>, IEmailCreator<EmailData>>();
	
	@Autowired
	@Qualifier("applicationConfig")
	private Properties configuration;

	@Value("#{applicationConfig['mail.FROM.default']}")
	private String defaultFrom;
	
	@Autowired
	@Qualifier("mainMailSender")
	private JavaMailSender sender;
	
	public MailSenderImpl() {
	}

	public Properties getConfiguration() {
		return configuration;
	}
	
	@Override
	public <T extends EmailData> void send(T correo) throws MailException {
		try {
			LOGGER.debug("Correo BEGIN  : {}", correo);
			sendInternal(correo);
			LOGGER.debug("Correo END    : {}", correo);
			
			// NOSONAR Perfectamente valido, ya que se quiere evitar
			// Que el fallo de correo afecte al resto
		} catch (Throwable t) {
			LOGGER.warn("Correo ERROR  : {}", correo, t);
			if (t instanceof Error) {
				throw (Error) t;
			} else if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new MailException("No se puede enviar el correo", t);
			}
		}
	}

	public Map<Class<EmailData>, IEmailCreator<EmailData>> getCreators() {
		return creators;
	}
	
	public void setConfiguration(Properties configuration) {
		this.configuration = configuration;
	}

	public void setCreators(Map<Class<EmailData>, IEmailCreator<EmailData>> creators) {
		this.creators = creators;
	}
	
	public void setSender(JavaMailSender sender) {
		this.sender = sender;
	}
	
	@SuppressWarnings("unchecked")
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.creators = (Map<Class<EmailData>, IEmailCreator<EmailData>>) applicationContext.getBean("mainMailCreatorsMap");
	}

	/*
	 * PRIVATE METHODS
	 */
	/**
	 * 
	 * @param original
	 * @param toRemove
	 * @return
	 */
	private Address[] filterAddresses(Address[] original, Set<String> toRemove) {
		List<Address> newAdresses = new ArrayList<Address>();
		boolean removed = false;
		if (original == null) {
			return null;
		}
		if (original.length == 0) {
			return original;
		}

		for (Address addr : original) {
			InternetAddress ia = (InternetAddress) addr;
			String email = ia.getAddress();
			if (toRemove.contains(email)) {
				removed = true;
				continue;
			}
			toRemove.add(email);
			newAdresses.add(addr);
		}

		if (removed) {
			Address[] addrs = newAdresses.toArray(new Address[newAdresses.size()]);
			return addrs;

		}
		return original;

	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T extends EmailData> IEmailCreator<T> selectEmailCreator(T data) {
		return (IEmailCreator<T>) creators.get(data.getClass());
	}
	
	/**
	 * 
	 * @param correo
	 * @throws Exception
	 */
	private <T extends EmailData> void sendInternal(T correo) throws Exception {

		IEmailCreator<T> creator = selectEmailCreator(correo);
		if (creator == null) {
			throw new ServiceException("No existe creador de mensajes para " + correo.getClass().getName());
		}

		MimeMessage message;

		message = sender.createMimeMessage();

		boolean prepared = creator.createMail(message, correo);
		if (!prepared) {
			return;
		}

		// No enviar correos sin To
		Address[] tos = message.getRecipients(RecipientType.TO);
		if (tos == null || tos.length == 0) {
			LOGGER.warn("Ignorando correo porque no tiene TO:" + correo);
			return;
		}

		message.setFrom(new InternetAddress(defaultFrom));
		
		removeDuplicatedRecipients(message);

		sender.send(message);
	}
	
	/**
	 * 
	 * @param message
	 * @throws MessagingException
	 */
	private void removeDuplicatedRecipients(MimeMessage message) throws MessagingException {
		Set<String> adresses = new TreeSet<String>();

		Address[] to = message.getRecipients(RecipientType.TO);
		message.setRecipients(RecipientType.TO, filterAddresses(to, adresses));

		Address[] cc = message.getRecipients(RecipientType.CC);
		message.setRecipients(RecipientType.CC, filterAddresses(cc, adresses));

		Address[] bcc = message.getRecipients(RecipientType.BCC);
		message.setRecipients(RecipientType.BCC, filterAddresses(bcc, adresses));

	}
}
