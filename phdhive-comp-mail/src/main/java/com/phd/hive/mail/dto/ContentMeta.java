package com.phd.hive.mail.dto;

import static com.phd.hive.mail.dto.ContentMetaAttributes.*;

/**
 * 
 * Metadatos de un contenido
 * 
 * @author ADORU3N
 * 
 */
@SuppressWarnings("serial")
public class ContentMeta extends BaseDto {

	private ContentMetaAttributes attributes ;

	private String id;

	public ContentMeta() {

	}

	public ContentMeta(ContentMetaAttributes attributes) {
		super();
		this.attributes = attributes;
	}

	public Object getAttribute(String name) {
		if (attributes == null) {
			return null;
		}
		return attributes.getAttribute(name);
	}

	public <T extends Object> T getAttribute(String name, Class<T> returnedClass) {
		if (attributes == null) {
			return null;
		}
		return attributes.getAttribute(name, returnedClass);
	}

	public ContentMetaAttributes getAttributes() {
		return attributes;
	}

	public String getId() {
		return id;
	}

	public String getMimeType() {
		return getAttribute(ATT_MIME_TYPE, String.class);
	}

	public String getOriginalName() {
		return getAttribute(ATT_ORIGINAL_FILENAME, String.class);
	}

	public String getOriginalNameExtension() {
		String originalName = getOriginalName() ;
		if (originalName == null) {
			return "";
		}
		int pos = originalName.lastIndexOf('.');
		if (pos == -1) {
			return "";
		}

		String ext = originalName.substring(pos + 1);
		return ext;
	}

	public void setAttribute(String name, Object value) {
		if (attributes == null) {
			attributes = new ContentMetaAttributes();
		}
		attributes.setAttribute(name, value);
	}

	public void setId(String id) {
		this.id = id;
		setAttribute(ATT_ID, id);
	}

	public void setMimeType(String mimeType) {
		setAttribute(ATT_MIME_TYPE, mimeType);
	}

	public void setOriginalName(String originalName) {
		setAttribute(ATT_ORIGINAL_FILENAME, originalName);
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 * 
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
		final String TAB = "    ";

		String retValue = "";

		retValue = "ContentMeta ( id = " + getId() + TAB + "originalName = "
				+ getOriginalName() + TAB + "mimeType = " + getMimeType() + TAB + " )";

		return retValue;
	}
	
}
