package com.phd.hive.mail.creator.freemarker;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import freemarker.core.Environment;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 
 * @author ADORU3N
 *
 */
public class SubjectTemplateDirectiveModel implements TemplateDirectiveModel {

	public static final String CUSTOM_ATTRIBUTE_SUBJECT = "__SUBJECT__";
	
	@SuppressWarnings({ "rawtypes" })
	public void execute(Environment env, Map params, TemplateModel[] loopvars,
			TemplateDirectiveBody body) throws TemplateException, IOException {

		if (body == null) {
			return;
		}
		boolean overwrite = true ;
		Object over = params.get("overwrite") ;
		if(over != null) {
			if (over instanceof TemplateBooleanModel) {
				TemplateBooleanModel booleanModel = (TemplateBooleanModel) over;
				overwrite= booleanModel.getAsBoolean() ;
			}
		}
		boolean alreadySet = false ;
		String subject = (String) env.getCustomAttribute(CUSTOM_ATTRIBUTE_SUBJECT) ;
		if (StringUtils.isNotBlank(subject)) {
			alreadySet = true ;
		}
		
		if(alreadySet && ! overwrite) {
			// No sobreescribir
			return;
		}
		StringWriter wr = new StringWriter();
		body.render(wr);
		wr.close();
		
		StringBuffer sb = wr.getBuffer();
		subject = sb.toString();
		subject = subject.replace('\n', ' ');

		if (StringUtils.isNotBlank(subject)) {
			subject = subject.trim();
			env.setCustomAttribute(CUSTOM_ATTRIBUTE_SUBJECT, subject);
			env.setGlobalVariable("subject_value", new SimpleScalar(subject)) ;
		}

	}

}
