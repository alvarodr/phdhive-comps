package com.phd.hive.mail;

import com.phd.hive.mail.exception.MailException;
import com.phd.hive.mail.model.EmailData;

/**
 * Servicio para el envio de mails.
 * 
 * @author ADORU3N
 *
 */
public interface IMailService {

	/**
	 * Envia un correo.
	 * <BR>
	 * 
	 * El comportamiento de este metodo es el siguiente:<br>
	 * <br>
	 * 
	 * Si existe una transaccion activa, al realizarse un commit con exito, se
	 * envia el correo a una cola de envio. De forma asincrona, en un momento
	 * futuro, se enviara el correo.<BR>
	 * Si no existe transaccion activa, se envia el correo a la cola de correo,
	 * para enviar en un futuro.<BR>
	 * <br>
	 * 
	 * Si no se puede enviar el correo, no se hace nada, salvo quiza dejar
	 * trazas. <br>
	 * 
	 * Es equivalente a sendMailOnTransaction(correo, true)
	 * <BR><BR>
	 * 
	 * @param correo
	 *            El modelo de correo
	 */
	<T extends EmailData> void sendMail(T correo);

	/**
	 * Se envia un correo a la cola de envio para que se envie en un futuro y
	 * sale inmediatamente.
	 * <BR><BR>
	 * 
	 * @param correo
	 *            El modelo de correo
	 */
	<T extends EmailData> void sendMailAsync(T correo);

	/**
	 * 
	 * Se envia el correo inmediatemente en el hilo que lo invoca. 
	 * <BR>
	 * Este metodo es bloqueante y espera a que se envie el correo.<BR>
	 * <BR>
	 * <B>NO RECOMENDADO</B> salvo para casos muy especifcos.
	 * <BR><BR>
	 * 
	 * @param correo
	 *            El modelo de correo
	 */
	<T extends EmailData> void sendMailInThread(T correo) throws MailException;

	/**
	 * Envia un correo.
	 * <BR>
	 * 
	 * El comportamiento de este metodo es el siguiente:<br>
	 * <br>
	 * 
	 * Si existe una transaccion activa, al realizarse un commit, y dependiendo
	 * del valor del parametro onlyOnCommit, solo en commit exitoso o , se envia
	 * el correo a una cola de envio. De forma asincrona, en un momento futuro,
	 * se enviara el correo.<BR>
	 * Si no existe transaccion activa, se envia el correo a la cola de correo,
	 * para enviar en un futuro.<BR>
	 * <br>
	 * 
	 * Si no se puede enviar el correo, no se hace nada, salvo quiza dejar
	 * trazas. 
	 * <BR><BR>
	 * 
	 * @param correo
	 *            el modelo de correo
	 * 
	 * @param onlyOncommit
	 *            True si se ha de enviar el correo solo cuando haya commit con
	 *            exito o false si se ha de enviar independientemente del exito
	 *            o fracaso de la transaccion
	 */
	<T extends EmailData> void sendMailOnTransaction(T correo, boolean onlyOncommit);
	
}
