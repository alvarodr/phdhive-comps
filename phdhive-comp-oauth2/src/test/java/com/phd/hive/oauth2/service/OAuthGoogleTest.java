package com.phd.hive.oauth2.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.phd.hive.oauth2.api.google.APIGoogle;
import com.phd.hive.oauth2.base.BaseSpringTest;
import com.phd.hive.oauth2.client.service.IOAuthService;
import com.phd.hive.oauth2.model.OAuthRequest;
import com.phd.hive.oauth2.model.Response;
import com.phd.hive.oauth2.model.Token;
import com.phd.hive.oauth2.model.Verifier;
import com.phd.hive.oauth2.types.TypeVerb;

public class OAuthGoogleTest extends BaseSpringTest {

	@Autowired
	private APIGoogle apiGoogle;
	
	@Autowired
	private IOAuthService oAuthService;
	
	@Test
	public void oauthGoogleTest() {
		Assert.assertNotNull(apiGoogle.getAccessTokenEndpoint());
		System.out.println(apiGoogle.getAuthorizationUrl());
		oAuthService.createInstance(apiGoogle);
		Verifier verifier = new Verifier("4/HWfdMLp8H_JOCtYEImN--n61b8Y_.AnqkUkL3_fsSYKs_1NgQtmVdr1rqiAI");
		Token accessToken = oAuthService.getAccessToken(verifier);
		// getting user profile
		OAuthRequest oauthRequest = new OAuthRequest(TypeVerb.GET, "https://www.googleapis.com/oauth2/v1/userinfo?alt=json");
		oAuthService.signRequest(accessToken, oauthRequest);
		Response oauthResponse = oauthRequest.send();
		oauthResponse.getBody();
	}
	
}
