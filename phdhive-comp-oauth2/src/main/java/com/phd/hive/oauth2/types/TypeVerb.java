package com.phd.hive.oauth2.types;

public enum TypeVerb {

	POST("POST"),
	PUT("PUT"),
	GET("GET");
	
	private String verb;
	
	private TypeVerb(String verb) {
		this.verb = verb;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}
	
}
