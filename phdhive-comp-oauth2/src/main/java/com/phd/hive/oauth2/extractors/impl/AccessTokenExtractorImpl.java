package com.phd.hive.oauth2.extractors.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.phd.hive.oauth2.exceptions.OAuthException;
import com.phd.hive.oauth2.extractors.IAccessTokenExtractor;
import com.phd.hive.oauth2.model.Token;
import com.phd.hive.oauth2.util.OAuthEncoder;
import com.phd.hive.oauth2.util.OAuthPreconditions;

/**
 * 
 * @author ADORU3N
 * 
 */
@Service("accessTokenExtractor")
public class AccessTokenExtractorImpl implements IAccessTokenExtractor {

	private static final String TOKEN_REGEX = "access_token=([^&]+)";
	private static final String EMPTY_SECRET = "";

	/*
	 * (non-Javadoc)
	 * @see com.phd.hive.oauth2.extractors.IAccessTokenExtractor#extract(java.lang.String)
	 */
	public Token extract(String response) {
		OAuthPreconditions.checkEmptyString(response, "Response body is incorrect. Can't extract a token from an empty string");

		// Standard regex
		Matcher matcher = Pattern.compile(TOKEN_REGEX).matcher(response);
		if (matcher.find()) {
			String token = OAuthEncoder.decode(matcher.group(1));
			return new Token(token, EMPTY_SECRET, response);
		} else {
			// Google regex
			response = response.replaceAll("\":\"", "\" : \"");
			matcher = Pattern.compile("\"access_token\" : \"([^&\"]+)\"").matcher(response);
			if (matcher.find()) {
                String token = OAuthEncoder.decode(matcher.group(1));
                return new Token(token, EMPTY_SECRET, response);
			} else {
				throw new OAuthException("Response body is incorrect. Can't extract a token from this: '" + response + "'", null);
			}
		}
	}
}
