package com.phd.hive.oauth2.api.base;

import java.io.UnsupportedEncodingException;

import com.phd.hive.oauth2.api.IAPI;
import com.phd.hive.oauth2.types.TypeVerb;
import com.phd.hive.oauth2.util.OAuthConstants;

/**
 * Default API factory for OAuth 2.0.
 * 
 * @author ADORU3N
 * 
 */
public abstract class DefaultAPI20 implements IAPI {

	protected String apiKey;
	protected String apiSecret;
	protected String callback;
	protected String scope;
	protected String authorizationURL;
	protected String endpoint;
	protected String prefix;

	/**
	 * Returns the verb for the access token endpoint (defaults to GET)
	 * 
	 * @return access token endpoint verb
	 */
	public TypeVerb getAccessTokenVerb() {
		return TypeVerb.GET;
	}

	/**
	 * 
	 * @return
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * 
	 * @param apiKey
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * 
	 * @return
	 */
	public String getApiSecret() {
		return apiSecret;
	}

	/**
	 * 
	 * @param apiSecret
	 */
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	/**
	 * 
	 * @return
	 */
	public String getCallback() {
		return callback;
	}

	/**
	 * 
	 * @param callback
	 */
	public void setCallback(String callback) {
		this.callback = callback;
	}

	/**
	 * 
	 * @return
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * 
	 * @param scope
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * 
	 * @return
	 */
	public String getAuthorizationURL() {
		return authorizationURL;
	}

	/**
	 * 
	 * @param authorizationURL
	 */
	public void setAuthorizationURL(String authorizationURL) {
		this.authorizationURL = authorizationURL;
	}

	/**
	 * 
	 * @return
	 */
	public String getEndpoint() {
		return endpoint;
	}

	/**
	 * 
	 * @param endpoint
	 */
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	/**
	 * Parametro de envio
	 * @return
	 */
	public String getPrefix() {
		return OAuthConstants.ACCESS_TOKEN;
	}

	/**
	 * Returns the URL that receives the access token requests.
	 * 
	 * @return access token URL
	 */
	public abstract String getAccessTokenEndpoint();

	/**
	 * Returns the URL where you should redirect your users to authenticate your
	 * application.
	 * 
	 * @param config
	 *            OAuth 2.0 configuration param object
	 * @return the URL where you should redirect your users
	 * @throws UnsupportedEncodingException 
	 */
	public abstract String getAuthorizationUrl() throws UnsupportedEncodingException;

}
