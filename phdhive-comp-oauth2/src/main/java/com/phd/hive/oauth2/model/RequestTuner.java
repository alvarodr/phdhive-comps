package com.phd.hive.oauth2.model;

/**
 * 
 * @author ADORU3N
 *
 */
public abstract class RequestTuner {
	public abstract void tune(Request request);
}
