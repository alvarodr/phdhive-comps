package com.phd.hive.oauth2.api.paypal;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.phd.hive.oauth2.api.base.DefaultAPI20;
import com.phd.hive.oauth2.util.OAuthEncoder;

/**
 * 
 * @author ADORU3N
 *
 */
@Component("paypalAPI")
public class APIPayPal extends DefaultAPI20 {

	@Value("#{applicationConfig['app.config.oauth.paypal.apikey']}")
	public void setApiKey(String apiKey) {this.apiKey = apiKey;};

	@Value("#{applicationConfig['app.config.oauth.paypal.apisecret']}")
	public void setApiSecret(String apiSecret) {this.apiSecret = apiSecret;};
	
	@Value("#{applicationConfig['app.config.oauth.paypal.callback']}")
	public void setCallback(String callback) {this.callback = callback;};
	
	@Value("#{applicationConfig['app.config.oauth.paypal.scope']}")
	public void setScope(String scope) {this.scope = scope;};
	
	@Value("#{applicationConfig['app.config.oauth.paypal.authURL']}")
	public void setAuthorizationURL(String authorizationURL) {this.authorizationURL = authorizationURL;};
	
	@Value("#{applicationConfig['app.config.oauth.paypal.endpoint']}")
	public void setEndpoint(String endpoint) {this.endpoint = endpoint;};
	
	@Override
	public String getAccessTokenEndpoint() {
		return getEndpoint();
	}

	@Override
	public String getAuthorizationUrl() throws UnsupportedEncodingException {
		return String.format(getAuthorizationURL(), getApiKey(), URLEncoder.encode(getCallback(), "UTF-8"), OAuthEncoder.encode(getScope()));
	}

}
