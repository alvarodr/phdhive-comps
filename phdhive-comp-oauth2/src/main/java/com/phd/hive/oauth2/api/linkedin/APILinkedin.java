package com.phd.hive.oauth2.api.linkedin;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.phd.hive.oauth2.api.base.DefaultAPI20;
import com.phd.hive.oauth2.util.OAuthConstants;
import com.phd.hive.oauth2.util.OAuthEncoder;

/**
 * 
 * @author ADORU3N
 *
 */
@Component("linkedinAPI")
public class APILinkedin extends DefaultAPI20 {

	@Value("#{applicationConfig['app.config.oauth.linkedin.apikey']}")
	public void setApiKey(String apiKey) {this.apiKey = apiKey;};

	@Value("#{applicationConfig['app.config.oauth.linkedin.apisecret']}")
	public void setApiSecret(String apiSecret) {this.apiSecret = apiSecret;};
	
	@Value("#{applicationConfig['app.config.oauth.linkedin.callback']}")
	public void setCallback(String callback) {this.callback = callback;};
	
	@Value("#{applicationConfig['app.config.oauth.linkedin.scope']}")
	public void setScope(String scope) {this.scope = scope;};
	
	@Value("#{applicationConfig['app.config.oauth.linkedin.authURL']}")
	public void setAuthorizationURL(String authorizationURL) {this.authorizationURL = authorizationURL;};
	
	@Value("#{applicationConfig['app.config.oauth.linkedin.endpoint']}")
	public void setEndpoint(String endpoint) {this.endpoint = endpoint;};
	
	@Override
	public String getAccessTokenEndpoint() {
		return getEndpoint();
	}

	@Override
	public String getAuthorizationUrl() {
		return String.format(getAuthorizationURL(), getApiKey(), UUID.randomUUID(), OAuthEncoder.encode(getCallback()), OAuthEncoder.encode(getScope()));
	}

	@Override
	public String getPrefix() {
		return OAuthConstants.ACCESS_TOKEN_OAUTH2;
	}
}
