package com.phd.hive.oauth2.client.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phd.hive.oauth2.api.base.DefaultAPI20;
import com.phd.hive.oauth2.api.google.APIGoogle;
import com.phd.hive.oauth2.api.linkedin.APILinkedin;
import com.phd.hive.oauth2.api.paypal.APIPayPal;
import com.phd.hive.oauth2.client.service.IOAuthService;
import com.phd.hive.oauth2.extractors.IAccessTokenExtractor;
import com.phd.hive.oauth2.model.OAuthRequest;
import com.phd.hive.oauth2.model.Response;
import com.phd.hive.oauth2.model.Token;
import com.phd.hive.oauth2.model.Verifier;
import com.phd.hive.oauth2.util.OAuthConstants;

@Service("OAuth20Service")
public class OAuth20ServiceImpl implements IOAuthService {

	@Autowired
	private IAccessTokenExtractor accessTokenExtractor; 
	
	private DefaultAPI20 api;

	/*
	 * (non-Javadoc)
	 * @see com.phd.hive.oauth2.client.service.IOAuthService#createInstance(com.phd.hive.oauth2.api.base.DefaultAPI20, com.phd.hive.oauth2.client.config.OAuthConfig)
	 */
	public void createInstance(DefaultAPI20 api) {
		this.api = api;
	}

	/*
	 * (non-Javadoc)
	 * @see com.phd.hive.oauth2.client.service.IOAuthService#signRequest(com.phd.hive.oauth2.model.Token, com.phd.hive.oauth2.model.OAuthRequest)
	 */
	public void signRequest(Token accessToken, OAuthRequest request) {
		request.addQuerystringParameter(api.getPrefix(), accessToken.getToken());
	}

	/*
	 * (non-Javadoc)
	 * @see com.phd.hive.oauth2.client.service.IOAuthService#getAccessToken(com.phd.hive.oauth2.model.Verifier)
	 */
	public Token getAccessToken(Verifier verifier) {
		OAuthRequest request = new OAuthRequest(api.getAccessTokenVerb(), api.getAccessTokenEndpoint());
		if (api instanceof APIGoogle || api instanceof APILinkedin) {
			request.addBodyParameter(OAuthConstants.CLIENT_ID, api.getApiKey());
			request.addBodyParameter(OAuthConstants.CLIENT_SECRET, api.getApiSecret());
			request.addBodyParameter(OAuthConstants.CODE, verifier.getValue());
			request.addBodyParameter(OAuthConstants.REDIRECT_URI, api.getCallback());
			request.addBodyParameter(OAuthConstants.GRANT_TYPE, OAuthConstants.GRANT_TYPE_AUTHORIZATION_CODE);
			request.addBodyParameter(OAuthConstants.SCOPE, api.getScope());
		} else if (api instanceof APIPayPal){
			request.addBodyParameter(OAuthConstants.CLIENT_ID, api.getApiKey());
			request.addBodyParameter(OAuthConstants.GRANT_TYPE, OAuthConstants.GRANT_TYPE_CLIENT_CREDENTIALS);
			request.addBodyParameter(OAuthConstants.SCOPE, api.getScope());
			request.addBodyParameter(OAuthConstants.REDIRECT_URI, api.getCallback());
		} else {
			request.addQuerystringParameter(OAuthConstants.CLIENT_ID, api.getApiKey());
			request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET, api.getApiSecret());
			request.addQuerystringParameter(OAuthConstants.CODE, verifier.getValue());
			request.addQuerystringParameter(OAuthConstants.REDIRECT_URI, api.getCallback());
			request.addQuerystringParameter(OAuthConstants.SCOPE, api.getScope());
		}

		if (api instanceof APILinkedin) {
			request.addQuerystringParameter(OAuthConstants.GRANT_TYPE, OAuthConstants.GRANT_TYPE_AUTHORIZATION_CODE);
		}
		
		Response response = request.send();
		return accessTokenExtractor.extract(response.getBody());
	}
}
