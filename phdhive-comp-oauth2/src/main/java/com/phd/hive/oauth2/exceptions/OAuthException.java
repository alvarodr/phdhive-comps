package com.phd.hive.oauth2.exceptions;

/**
 * Manage exception OAuth 2.0
 * @author ADORU3N
 *
 */
public class OAuthException extends RuntimeException {

	private static final long serialVersionUID = 6400037635894872242L;

	/**
	 * Default constructor
	 * @param message
	 * @param e
	 */
	public OAuthException(String message, Exception e) {
		super(message, e);
	}
	
	/**
	 * Default constructor without exception
	 * @param message
	 */
	public OAuthException(String message) {
		super(message);
	}
}
