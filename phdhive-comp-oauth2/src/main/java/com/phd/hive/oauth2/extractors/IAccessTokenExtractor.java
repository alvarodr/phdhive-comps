package com.phd.hive.oauth2.extractors;

import com.phd.hive.oauth2.model.Token;

/**
 * 
 * @author ADORU3N
 *
 */
public interface IAccessTokenExtractor {

	/**
	 * Extract token from response
	 * @param response
	 * @return
	 */
	Token extract(String response);
	
}
