package com.phd.hive.oauth2.client.service;

import com.phd.hive.oauth2.api.base.DefaultAPI20;
import com.phd.hive.oauth2.model.OAuthRequest;
import com.phd.hive.oauth2.model.Token;
import com.phd.hive.oauth2.model.Verifier;

/**
 * 
 * @author ADORU3N
 * 
 */
public interface IOAuthService {

	/**
	 * Create new instance
	 * 
	 * @return
	 */
	void createInstance(DefaultAPI20 api);

	/**
	 * Retrieve the access token
	 * 
	 * @param requestToken request token (obtained previously)
	 * @param verifier verifier code
	 * @return access token
	 */
	public Token getAccessToken(Verifier verifier);

	/**
	 * Signs am OAuth request
	 * 
	 * @param accessToken access token (obtained previously)
	 * @param request request to sign
	 */
	public void signRequest(Token accessToken, OAuthRequest request);
}
