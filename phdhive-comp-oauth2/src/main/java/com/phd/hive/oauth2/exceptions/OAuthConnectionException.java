package com.phd.hive.oauth2.exceptions;

/**
 * 
 * @author ADORU3N
 *
 */
public class OAuthConnectionException extends OAuthException {

	private static final long serialVersionUID = 8144381301227077607L;
	
	private static final String MSG = "There was a problem while creating a connection to the remote service.";

	public OAuthConnectionException(Exception e) {
		super(MSG, e);
	}
}
