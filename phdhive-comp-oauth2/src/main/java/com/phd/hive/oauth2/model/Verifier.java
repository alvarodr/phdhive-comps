package com.phd.hive.oauth2.model;

import com.phd.hive.oauth2.util.OAuthPreconditions;

/**
 * 
 * @author ADORU3N
 *
 */
public class Verifier {
	private final String value;

	/**
	 * Default constructor.
	 * 
	 * @param value
	 *            verifier value
	 */
	public Verifier(String value) {
		OAuthPreconditions.checkNotNull(value, "Must provide a valid string as verifier");
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
