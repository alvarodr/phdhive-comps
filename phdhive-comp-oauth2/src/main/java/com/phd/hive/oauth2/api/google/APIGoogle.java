package com.phd.hive.oauth2.api.google;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.phd.hive.oauth2.api.base.DefaultAPI20;
import com.phd.hive.oauth2.types.TypeVerb;
import com.phd.hive.oauth2.util.OAuthEncoder;

/**
 * 
 * @author ADORU3N
 *
 */
@Component("googleAPI")
public class APIGoogle extends DefaultAPI20 {

	@Value("#{applicationConfig['app.config.oauth.google.apikey']}")
	public void setApiKey(String apiKey) {this.apiKey = apiKey;};

	@Value("#{applicationConfig['app.config.oauth.google.apisecret']}")
	public void setApiSecret(String apiSecret) {this.apiSecret = apiSecret;};
	
	@Value("#{applicationConfig['app.config.oauth.google.callback']}")
	public void setCallback(String callback) {this.callback = callback;};
	
	@Value("#{applicationConfig['app.config.oauth.google.scope']}")
	public void setScope(String scope) {this.scope = scope;};
	
	@Value("#{applicationConfig['app.config.oauth.google.authURL']}")
	public void setAuthorizationURL(String authorizationURL) {this.authorizationURL = authorizationURL;};
	
	@Value("#{applicationConfig['app.config.oauth.google.endpoint']}")
	public void setEndpoint(String endpoint) {this.endpoint = endpoint;};
	
	@Override
	public String getAccessTokenEndpoint() {
		return getEndpoint();
	}

	@Override
	public String getAuthorizationUrl() {
		return String.format(getAuthorizationURL(), getApiKey(),OAuthEncoder.encode(getCallback()), OAuthEncoder.encode(getScope()));
	}

	@Override
	public TypeVerb getAccessTokenVerb() {
        return TypeVerb.POST;
    }
}
