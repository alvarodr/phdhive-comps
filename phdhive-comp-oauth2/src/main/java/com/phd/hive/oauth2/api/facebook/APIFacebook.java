package com.phd.hive.oauth2.api.facebook;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.phd.hive.oauth2.api.base.DefaultAPI20;
import com.phd.hive.oauth2.util.OAuthEncoder;

/**
 * 
 * @author ADORU3N
 *
 */
@Component("facebookAPI")
public class APIFacebook extends DefaultAPI20 {

	@Value("#{applicationConfig['app.config.oauth.facebook.apikey']}")
	public void setApiKey(String apiKey) {this.apiKey = apiKey;};

	@Value("#{applicationConfig['app.config.oauth.facebook.apisecret']}")
	public void setApiSecret(String apiSecret) {this.apiSecret = apiSecret;};
	
	@Value("#{applicationConfig['app.config.oauth.facebook.callback']}")
	public void setCallback(String callback) {this.callback = callback;};
	
	@Value("#{applicationConfig['app.config.oauth.facebook.scope']}")
	public void setScope(String scope) {this.scope = scope;};
	
	@Value("#{applicationConfig['app.config.oauth.facebook.authURL']}")
	public void setAuthorizationURL(String authorizationURL) {this.authorizationURL = authorizationURL;};
	
	@Value("#{applicationConfig['app.config.oauth.facebook.endpoint']}")
	public void setEndpoint(String endpoint) {this.endpoint = endpoint;};
	
	@Override
	public String getAccessTokenEndpoint() {
		return getEndpoint();
	}

	@Override
	public String getAuthorizationUrl() {
		return String.format(getAuthorizationURL(), getApiKey(), OAuthEncoder.encode(getCallback()), OAuthEncoder.encode(getScope()));
	}

}
