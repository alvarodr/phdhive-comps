package com.phd.hive.arch.transaction.sync;

import java.lang.reflect.Method;

import javax.transaction.TransactionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TxSynchronizationManagerFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(TxSynchronizationManagerFactory.class);

	private TransactionManager jtaTransactionManager;

	private Object jtaTransactionSyncronizationRegistry;

	public TransactionManager getJtaTransactionManager() {
		return jtaTransactionManager;
	}

	public Object getJtaTransactionSyncronizationRegistry() {
		return jtaTransactionSyncronizationRegistry;
	}

	public Object getObject() throws Exception {
		TxSynchronizationManager manager = null;

		if (jtaTransactionSyncronizationRegistry != null) {
			try {
				// Use JTA 1.1
				// Por reflection para evitar dependencia de binarios
				LOGGER.info("Usando JTA 1.1");
				manager = newInstance("Jta11TxSynchronizationManager");
				setProperty(manager, "setJtaTransactionSynchronizationRegistry", jtaTransactionSyncronizationRegistry);
			} catch (Exception ex) {
				LOGGER.warn("No se puede encontrar JTA 1.1", ex);
				manager = null;
			}
		}

		if (jtaTransactionManager != null) {
			try {
				// Por reflection para evitar dependencia de binarios
				// USe JTA 1.0
				LOGGER.info("Usando JTA 1.0 (Thread holders)");
				manager = newInstance("Jta10TxSynchronizationManager");
				setProperty(manager, "setJtaTransactionManager", jtaTransactionManager);
			} catch (Exception ex) {
				LOGGER.warn("No se puede encontrar JTA 1.0", ex);
				manager = null;
			}
		}
		if (manager != null) {
			return manager;
		}

		LOGGER.info("Usando Spring (Thread holders)");
		// Por reflection para evitar dependencia de binarios
		return newInstance("SpringTxSynchronizationManager");

	}

	@SuppressWarnings({"rawtypes" })
	public Class getObjectType() {
		return TxSynchronizationManager.class;
	}

	public boolean isSingleton() {
		return true;
	}

	@SuppressWarnings("unchecked")
	private TxSynchronizationManager newInstance(String clazzName) throws Exception {
		String name = getClass().getPackage().getName() + "." + clazzName;
		Class<? extends TxSynchronizationManager> clazz = (Class<? extends TxSynchronizationManager>) Class.forName(name);
		return clazz.newInstance();
	}

	public void setJtaTransactionManager(TransactionManager jtaTransactionManager) {
		this.jtaTransactionManager = jtaTransactionManager;
	}

	public void setJtaTransactionSyncronizationRegistry(Object jtaTransactionSyncronizationRegistry) {
		this.jtaTransactionSyncronizationRegistry = jtaTransactionSyncronizationRegistry;
	}

	private void setProperty(TxSynchronizationManager manager, String methodName, Object value) throws Exception {
		Method[] methods = manager.getClass().getMethods();
		for (Method method : methods) {
			if (method.getName().equals(methodName)) {
				method.invoke(manager, value);
				break;
			}
		}
	}
}
