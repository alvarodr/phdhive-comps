package com.phd.hive.arch.transaction.sync;

import java.util.Map;
import java.util.TreeMap;

import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

/**
 * 
 * @author ADORU3N
 *
 */
public class Jta10TxSynchronizationManager implements TxSynchronizationManager {

	private static final ThreadLocal<Map<String, Object>> SYNCH_HOLDER = new ThreadLocal<Map<String, Object>>() {
		protected Map<String, Object> initialValue() {
			return new TreeMap<String, Object>();
		};
	};

	public TransactionManager jtaTransactionManager;

	@SuppressWarnings("unchecked")
	public <T> T getCustomObject(String name, Class<T> clazz) throws TxSynchronizationException {
		Map<String, Object> map = SYNCH_HOLDER.get();
		return (T) map.get(name);
	}

	public TransactionManager getJtaTransactionManager() {
		return jtaTransactionManager;
	}

	public boolean isActive() throws TxSynchronizationException {
		try {
			int transactionStatus = jtaTransactionManager.getStatus();
			return transactionStatus == Status.STATUS_ACTIVE;
		} catch (Exception ex) {
			throw new TxSynchronizationException(ex);
		}
	}

	public void putCustomObject(String name, Object object) throws TxSynchronizationException {
		Map<String, Object> map = SYNCH_HOLDER.get();
		if (object == null) {
			map.remove(name);
		} else {
			map.put(name, object);
		}

	}

	public void registerSynchronization(Synchronization currentSync) throws TxSynchronizationException {
		try {
			Transaction transaction = jtaTransactionManager.getTransaction();
			transaction.registerSynchronization(currentSync);
		} catch (Exception ex) {
			throw new TxSynchronizationException(ex);
		}

	}

	public void setJtaTransactionManager(TransactionManager jtaTransactionManager) {
		this.jtaTransactionManager = jtaTransactionManager;
	}

}
