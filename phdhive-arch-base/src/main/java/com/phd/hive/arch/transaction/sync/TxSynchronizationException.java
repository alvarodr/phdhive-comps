package com.phd.hive.arch.transaction.sync;

/**
 * 
 * @author ADORU3N
 *
 */
public class TxSynchronizationException extends Exception {

	private static final long serialVersionUID = -430515274765278299L;

	public TxSynchronizationException(String message) {
		super(message);
	}

	public TxSynchronizationException(String message, Throwable cause) {
		super(message, cause);
	}

	public TxSynchronizationException(Throwable cause) {
		super(cause);
	}

}
