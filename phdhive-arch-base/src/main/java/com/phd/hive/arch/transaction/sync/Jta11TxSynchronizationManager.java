package com.phd.hive.arch.transaction.sync;

import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.TransactionSynchronizationRegistry;

/**
 * 
 * @author ADORU3N
 *
 */
public class Jta11TxSynchronizationManager implements TxSynchronizationManager {
	private TransactionSynchronizationRegistry jtaTransactionSynchronizationRegistry;

	@SuppressWarnings("unchecked")
	public <T> T getCustomObject(String name, Class<T> clazz) throws TxSynchronizationException {
		return (T) jtaTransactionSynchronizationRegistry.getResource(name);
	}

	public TransactionSynchronizationRegistry getJtaTransactionSynchronizationRegistry() {
		return jtaTransactionSynchronizationRegistry;
	}

	public boolean isActive() throws TxSynchronizationException {
		try {
			int transactionStatus = jtaTransactionSynchronizationRegistry.getTransactionStatus();
			return transactionStatus == Status.STATUS_ACTIVE;
		} catch (Exception ex) {
			throw new TxSynchronizationException(ex);
		}
	}

	public void putCustomObject(String name, Object object) throws TxSynchronizationException {
		jtaTransactionSynchronizationRegistry.putResource(name, object);
	}

	public void registerSynchronization(Synchronization currentSync) throws TxSynchronizationException {
		try {
			jtaTransactionSynchronizationRegistry.registerInterposedSynchronization(currentSync);
		} catch (Exception ex) {
			throw new TxSynchronizationException(ex);
		}

	}

	public void setJtaTransactionSynchronizationRegistry(TransactionSynchronizationRegistry jtaTransactionSynchronizationRegistry) {
		this.jtaTransactionSynchronizationRegistry = jtaTransactionSynchronizationRegistry;
	}
}
