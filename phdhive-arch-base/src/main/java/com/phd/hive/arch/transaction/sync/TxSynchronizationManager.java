package com.phd.hive.arch.transaction.sync;

import javax.transaction.Synchronization;

/**
 * 
 * @author ADORU3N
 *
 */
public interface TxSynchronizationManager {
	/**
	 * @return true si hay alguna transaccion activa
	 * @throws TxSynchronizationException
	 *             si hay algun error intentando saber si hay alguna transaccion
	 *             activa
	 */
	boolean isActive() throws TxSynchronizationException;

	/**
	 * Registra una sincronizacion de transaccion
	 * 
	 * @param sync
	 *            sincronizacion
	 * @throws TxSynchronizationException
	 *             si hay algun error al registrar la sincronizacion
	 */
	void registerSynchronization(Synchronization sync) throws TxSynchronizationException;

	/**
	 * Recupera un objeto asociado a la transaccion
	 * 
	 * @param name
	 *            el nombre del objeto
	 * @param clazz
	 *            el tipo del objeto a recuperar
	 * @return el objeto o null si no lo hubiera
	 * @throws TxSynchronizationException
	 *             si hubo algun error
	 */
	<T> T getCustomObject(String name, Class<T> clazz) throws TxSynchronizationException;

	/**
	 * Almacena un objeto asociado a la transaccion
	 * 
	 * @param name
	 *            el nombre que se le dara al objeto
	 * @param object
	 *            el objeto a almacenar
	 * @throws TxSynchronizationException
	 *             si hubo algun error
	 */
	void putCustomObject(String name, Object object) throws TxSynchronizationException;
}
